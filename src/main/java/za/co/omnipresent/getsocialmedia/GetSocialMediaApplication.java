package za.co.omnipresent.getsocialmedia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetSocialMediaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetSocialMediaApplication.class, args);
	}

}
