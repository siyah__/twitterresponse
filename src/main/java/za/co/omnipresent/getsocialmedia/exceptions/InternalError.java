package za.co.omnipresent.getsocialmedia.exceptions;


public class InternalError extends RuntimeException{
    public InternalError(String s){super(s);}
}
