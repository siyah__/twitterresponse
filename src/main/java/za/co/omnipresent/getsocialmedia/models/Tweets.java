package za.co.omnipresent.getsocialmedia.models;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
public class Tweets implements Serializable {
    @Id
    private String id;

    private String topic;
    private String fullText;
    private String province;
    private String cityName;

    public Tweets(String topic, String fullText, String province, String cityName) {
        this.topic = topic;
        this.fullText = fullText;
        this.province = province;
        this.cityName = cityName;
    }

    public Tweets(){

    }
}
