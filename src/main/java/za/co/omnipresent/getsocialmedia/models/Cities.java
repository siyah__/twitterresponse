package za.co.omnipresent.getsocialmedia.models;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Cities {
    @Id
    private String id;
    private String cityName;
    private String province;
    private String coordinates;

    public Cities() {
    }

    public Cities(String cityName, String province, String coordinates) {
        this.cityName = cityName;
        this.province = province;
        this.coordinates = coordinates;
    }
}
