package za.co.omnipresent.getsocialmedia.models;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
public class Statuses implements Serializable {

    @Id
    private String id;
    @JsonAlias({"full_text"})
    private String fullText;
}