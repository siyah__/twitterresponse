package za.co.omnipresent.getsocialmedia.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;
import za.co.omnipresent.getsocialmedia.models.*;
import za.co.omnipresent.getsocialmedia.repository.CitiesRepository;
import za.co.omnipresent.getsocialmedia.repository.SocialAnalysisRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TwitterServiceImpl implements TwitterService {
    private final Logger logger = Logger.getLogger(TwitterServiceImpl.class.getName());
    private SocialAnalysisRepository socialAnalysisRepository;
    private CitiesRepository citiesRepository;
    private RestTemplate restTemplate;

    @Value("${twitter.api.consumer-key}")
    public String consumerKey;
    @Value("${twitter.api.consumer-secret}")
    public String consumerSecret;
    @Value("${twitter.api.access-token}")
    public String accessToken;
    @Value("${twitter.api.token-secret}")
    public String tokenSecret;
    @Value("${twitter.api.URL}")
    private String twitterURL;
    @Value("${sentiment.analysis.service.url}")
    private String sentimentUrl;
    @Value("${twitter.api.search-twitter-words}")
    private String twitterURLwithParams;


    @Override
    @ApiOperation("Using twitter search api to find tweets on specific topics.")
    public String getTweetsByTopicForEachCity(String topic) throws Exception
    {
        List<Cities> citiesList =  citiesRepository.findAll();
        if(!citiesList.isEmpty())
        {
            for(Cities city: citiesList)
            {
                List<Tweets> tweetsList = fetchTweetsFromTwitter(city, topic);
                socialAnalysisRepository.saveAll(tweetsList);
            }
            logger.log(Level.INFO, "tweets have been stored in the database successfully.");
            return "Tweets retrieved and stored in the database.";
        }
        throw new InternalError("The citiesRepository is empty");
    }

    public List<Tweets> fetchTweetsFromTwitter(Cities city, String topic) throws Exception {
        List<Tweets> tweetsList = new ArrayList<>();
        TweetsResponse response = mapTweetsToModel(city, topic);
        for(Statuses status: response.getStatuses())
        {
            tweetsList.add(new Tweets(topic, status.getFullText(), city.getProvince(), city.getCityName()));
        }
        return tweetsList;
    }

    public TweetsResponse mapTweetsToModel(Cities city, String topic) throws Exception {
        String urlWithCoordinatesAndTopic = twitterURLwithParams
                .replace("{params}", topic)
                .replace("{coordinates}", city.getCoordinates());
        String responseString = restTemplate.getForObject(urlWithCoordinatesAndTopic, String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return mapper.readValue(responseString, TweetsResponse.class);
    }

    @Autowired
    public void setSocialAnalysisRepository(SocialAnalysisRepository socialAnalysisRepository){
        this.socialAnalysisRepository = socialAnalysisRepository;
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    @Autowired
    public void setCitiesRepository(CitiesRepository citiesRepository)
    {
        this.citiesRepository = citiesRepository;
    }
}
