package za.co.omnipresent.getsocialmedia.service;

public interface TwitterService {
    String getTweetsByTopicForEachCity(String words) throws Exception;
}
