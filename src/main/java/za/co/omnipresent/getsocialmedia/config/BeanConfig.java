package za.co.omnipresent.getsocialmedia.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.web.client.RestTemplate;
import za.co.omnipresent.getsocialmedia.service.TwitterService;
import za.co.omnipresent.getsocialmedia.service.TwitterServiceImpl;

@Configuration
public class BeanConfig {
    @Value("${twitter.api.consumer-key}")
    public String consumerKey;
    @Value("${twitter.api.consumer-secret}")
    public String consumerSecret;
    @Value("${twitter.api.access-token}")
    public String accessToken;
    @Value("${twitter.api.token-secret}")
    public String tokenSecret;

    @Bean
    public RestTemplate getRestTemplate(){
        TwitterTemplate twitterTemplate = new TwitterTemplate(consumerKey, consumerSecret, accessToken, tokenSecret);
        return twitterTemplate.getRestTemplate();
    }

    @Bean
    public TwitterService twitterService(){
        return new TwitterServiceImpl();
    }

    @Bean
    public TwitterTemplate twitterTemplate(){
        return new TwitterTemplate(consumerKey, consumerSecret, accessToken, tokenSecret);
    }

}
