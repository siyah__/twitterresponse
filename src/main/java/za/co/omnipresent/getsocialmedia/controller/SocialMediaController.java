package za.co.omnipresent.getsocialmedia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.omnipresent.getsocialmedia.service.TwitterService;

@RestController
@RequestMapping(value = "/socials")
public class SocialMediaController {
    private TwitterService twitterService;

    @GetMapping(value = "/twitter/search/{topic}")
    public @ResponseBody String getTweetsByTopicForEachCity(@PathVariable String topic) throws Exception
    {
        return twitterService.getTweetsByTopicForEachCity(topic);
    }

    @Autowired
    public void setTwitterService(TwitterService twitterService) {
        this.twitterService = twitterService;
    }
}
