package za.co.omnipresent.getsocialmedia.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.getsocialmedia.models.Tweets;

public interface SocialAnalysisRepository extends MongoRepository<Tweets, String> {

}
