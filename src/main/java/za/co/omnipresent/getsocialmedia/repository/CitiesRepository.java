package za.co.omnipresent.getsocialmedia.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.getsocialmedia.models.Cities;

public interface CitiesRepository extends MongoRepository<Cities, String> {
}
