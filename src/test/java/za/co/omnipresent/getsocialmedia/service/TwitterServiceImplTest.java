package za.co.omnipresent.getsocialmedia.service;

import org.junit.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import za.co.omnipresent.getsocialmedia.models.Cities;
import za.co.omnipresent.getsocialmedia.models.Statuses;
import za.co.omnipresent.getsocialmedia.models.TweetsResponse;
import za.co.omnipresent.getsocialmedia.models.Tweets;
import za.co.omnipresent.getsocialmedia.repository.CitiesRepository;
import za.co.omnipresent.getsocialmedia.repository.SocialAnalysisRepository;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TwitterServiceImplTest {
    private TwitterServiceImpl twitterService;
    private RestTemplate restTemplate;
    @Autowired
    private SocialAnalysisRepository socialAnalysisRepository;
    @Autowired
    private CitiesRepository citiesRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setUp(){
        twitterService = new TwitterServiceImpl();
        MockitoAnnotations.initMocks(this);
        restTemplate = Mockito.mock(RestTemplate.class);
        twitterService.setRestTemplate(restTemplate);
        twitterService.setCitiesRepository(this.citiesRepository);
        twitterService.setSocialAnalysisRepository(this.socialAnalysisRepository);

        ReflectionTestUtils.setField(twitterService, "twitterURL", "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=TheDarkerLight");
        ReflectionTestUtils.setField(twitterService, "twitterURLwithParams", "https://api.twitter.com/1.1/search/tweets.json?q={params}&count=20&geocode={coordinates}&result_type=recent&tweet_mode=extended&lang=en");

    }
    @After
    public void clean(){
        mongoTemplate.dropCollection(CitiesRepository.class);
        mongoTemplate.dropCollection(SocialAnalysisRepository.class);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

 @Test
 @DisplayName("Given a topic, should find tweets that contain that topic by geolocation then store them in the database, with no errors.")
 public void shouldGetTweetsByTopic() throws Exception {
       String jsonString = returnJsonString();
       saveTweetsToRepo();
       saveCitiesToRepo();
       Mockito.when(restTemplate.getForObject(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(jsonString);

      String exp = "Tweets retrieved and stored in the database.";
      String actual = twitterService.getTweetsByTopicForEachCity("cars");
      Assert.assertEquals(exp, actual);
}

@Test
@DisplayName("Given a topic, when the database is empty, an internal error should be thrown.")
public void shouldThrowInternalError() throws Exception {
     thrown.expect(InternalError.class);
     thrown.expectMessage("The citiesRepository is empty");
     saveCitiesToRepo();
     this.citiesRepository.deleteAll();

     twitterService.getTweetsByTopicForEachCity("hello");
}

@Test
@DisplayName("Given a topic and Cities object, should fetch tweets from twitter and return a list that is not empty.")
public void shouldReturnAListThatIsNotEmpty() throws Exception{
     Cities city = createCity();
     String topic = "world";
     String jsonString = returnJsonString();
     Mockito.when(restTemplate.getForObject(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(jsonString);
     List<Tweets> tweetsList = twitterService.fetchTweetsFromTwitter(city, topic);
     Assert.assertFalse(tweetsList.isEmpty());
}

@Test
@DisplayName("Given a topic and Cities object, should fetch tweets from twitter, map them correctly and return list with correct values.")
public void shouldReturnCorrectList() throws Exception {
    Cities city = createCity();
    String topic = "world";
    String jsonString = returnJsonString();
    Mockito.when(restTemplate.getForObject(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(jsonString);
    List<Tweets> tweetsList = twitterService.fetchTweetsFromTwitter(city, topic);
    Tweets tweet1 = tweetsList.get(0);
    String expFullText = "RT libertyautoSA: 2008 #Toyota #Yaris 1.0 #3Doors T3 #Manual Transmission \n" +
            "95,000km #Hatch Back Cloth Upholstery, #Power Windows Great Buy (#White) \n" +
            "\n" +
            "NOW R59,900  WAS R75,900\n" +
            "\n" +
            "Over 12,000 #Bargain #Cars For #Sale at #Liberty #Auto #Mzanzi #SouthAfrica \n" +
            "\n" +
            "… https://t.co/96TT7FkYMV";
    Assert.assertEquals("world", tweet1.getTopic());
    Assert.assertEquals("Pretoria", tweet1.getCityName());
    Assert.assertEquals("Gauteng", tweet1.getProvince());
    Assert.assertEquals(expFullText, tweet1.getFullText());

    Tweets tweet2 = tweetsList.get(1);
    String expFullText2 = "RT libertyautoSA: 2002 #Toyota #TAZZ #Conquest 1600RS #Hatch Back \n" +
            "#Manual Transmission 180,000km Front #Wheel Drive Great Buy (#Silver) \n" +
            "\n" +
            "R55,900\n" +
            "\n" +
            "Over 12,000 #Bargain #Cars For #Sale at #Liberty #Auto #Mzanzi #SouthAfrica \n" +
            "\n" +
            "Whats App 074 | 095 | 6652 #… https://t.co/rcUPvbCxMi";
    Assert.assertEquals("world", tweet2.getTopic());
    Assert.assertEquals("Pretoria", tweet2.getCityName());
    Assert.assertEquals("Gauteng", tweet2.getProvince());
    Assert.assertEquals(expFullText2, tweet2.getFullText());
}

@Test
@DisplayName("Given Topic and Cities object, should retrieve tweets and map them correctly to TweetsResponse object and throw no Exceptions.")
public void shouldMapTweets() throws Exception {
    Cities city = createCity();
    String topic = "world";
    String jsonString = returnJsonString();
    Mockito.when(restTemplate.getForObject(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(jsonString);
    TweetsResponse response = twitterService.mapTweetsToModel(city, topic);
    List<Statuses> statuses = response.getStatuses();
    Statuses status1 = statuses.get(0);
    Statuses status2 = statuses.get(1);
    String expFullText1 = "RT libertyautoSA: 2008 #Toyota #Yaris 1.0 #3Doors T3 #Manual Transmission \n" +
            "95,000km #Hatch Back Cloth Upholstery, #Power Windows Great Buy (#White) \n" +
            "\n" +
            "NOW R59,900  WAS R75,900\n" +
            "\n" +
            "Over 12,000 #Bargain #Cars For #Sale at #Liberty #Auto #Mzanzi #SouthAfrica \n" +
            "\n" +
            "… https://t.co/96TT7FkYMV";
    String expFullText2 = "RT libertyautoSA: 2002 #Toyota #TAZZ #Conquest 1600RS #Hatch Back \n" +
            "#Manual Transmission 180,000km Front #Wheel Drive Great Buy (#Silver) \n" +
            "\n" +
            "R55,900\n" +
            "\n" +
            "Over 12,000 #Bargain #Cars For #Sale at #Liberty #Auto #Mzanzi #SouthAfrica \n" +
            "\n" +
            "Whats App 074 | 095 | 6652 #… https://t.co/rcUPvbCxMi";

    Assert.assertEquals(expFullText1, status1.getFullText());
    Assert.assertEquals(expFullText2, status2.getFullText());
}

@Test
@DisplayName("When given a topic that doesnt that doesnt exist, should return a json string with an empty statuses array.")
public void shouldNotFindTweets() throws Exception {
    Cities city = createCity();
    String topic = "assdass";
    String jsonString = returnJsonWithEmptyStatuses();
    Mockito.when(restTemplate.getForObject(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(jsonString);
    TweetsResponse response = twitterService.mapTweetsToModel(city, topic);

    Assert.assertTrue(response.getStatuses().isEmpty());
}

@Test
@DisplayName("Given a topic that doesnt exist, fetchTweetsFromTwitter method should return an empty array.")
public void shouldReturnEmptyArray() throws Exception{
    Cities city = createCity();
    String topic = "assdass";
    String jsonString = returnJsonWithEmptyStatuses();
    Mockito.when(restTemplate.getForObject(ArgumentMatchers.anyString(), ArgumentMatchers.any())).thenReturn(jsonString);
    List<Tweets> tweetsList = twitterService.fetchTweetsFromTwitter(city, topic);

    Assert.assertTrue(tweetsList.isEmpty());
}

private Cities createCity(){
     return new Cities("Pretoria", "Gauteng", "23.5111,25.4554,45km");
}

private TweetsResponse returnTweetsResponse(){
     TweetsResponse response = new TweetsResponse();
     response.setStatuses(createListOfStatuses());
     return response;
}

private List<Statuses> createListOfStatuses(){
     List<Statuses> statuses = new ArrayList<>();

     Statuses status1 = new Statuses();
     status1.setId("ss123");
     status1.setFullText("Hello world!!!");
     statuses.add(status1);

     Statuses status2 = new Statuses();
     status1.setId("ss1234");
     status1.setFullText("The world is very big");
     statuses.add(status2);

     return statuses;
}
private void saveTweetsToRepo(){
        List<Tweets> tweetsList = new ArrayList<>();

        Tweets tweets = new Tweets();
        tweets.setCityName("Joburg");
        tweets.setFullText("hello world");
        tweets.setId("sdfgssdfs");
        tweets.setProvince("Gauteng");
        tweetsList.add(tweets);

        Tweets tweets1 = new Tweets();
        tweets1.setCityName("Pretoria");
        tweets1.setFullText("hello world");
        tweets1.setId("sdfgssdfsasdas");
        tweets1.setProvince("Gauteng");
        tweetsList.add(tweets1);

        this.socialAnalysisRepository.saveAll(tweetsList);
    }

    private void saveCitiesToRepo(){
        List<Cities> citiesList = new ArrayList<>();

        Cities city1 = new Cities();
        city1.setCityName("Joburg");
        city1.setCoordinates("23.434234,12.23123,24km");
        city1.setProvince("Gauteng");
        citiesList.add(city1);

        Cities city2 = new Cities();
        city2.setCityName("Pretoria");
        city2.setCoordinates("27.434234,17.23123,24km");
        city2.setProvince("Gauteng");
        citiesList.add(city2);

        this.citiesRepository.saveAll(citiesList);
    }

    private String returnJsonWithEmptyStatuses(){
     return "{\n" +
             "    \"statuses\": [],\n" +
             "    \"search_metadata\": {\n" +
             "        \"completed_in\": 0.025,\n" +
             "        \"max_id\": 1230863677064122370,\n" +
             "        \"max_id_str\": \"1230863677064122370\",\n" +
             "        \"query\": \"sadasdaasad\",\n" +
             "        \"refresh_url\": \"?since_id=1230863677064122370&q=sadasdaasad&geocode=-26.276138500000002%2C28.00929511161209%2C30.37km&lang=en&result_type=recent&include_entities=1\",\n" +
             "        \"count\": 2,\n" +
             "        \"since_id\": 0,\n" +
             "        \"since_id_str\": \"0\"\n" +
             "    }\n" +
             "}";
    }

    private String returnJsonString()
    {
       return "{\"statuses\":[{\"created_at\":\"Fri Feb 21 08:39:22 +0000 2020\",\"id\":1230773998075531264,\"id_str\":\"1230773998075531264\"," +
               "\"full_text\":\"RT libertyautoSA: 2008 #Toyota #Yaris 1.0 #3Doors T3 #Manual Transmission \\n95,000km #Hatch Back Cloth Upholstery," +
               " #Power Windows Great Buy (#White) \\n\\nNOW R59,900  WAS R75,900\\n\\nOver 12,000 #Bargain #Cars For #Sale at #Liberty #Auto #Mzanzi #SouthAfrica \\n\\n\\u2026 https:\\/\\/t.co\\/96TT7FkYMV\",\"truncated\":false,\"display_text_range\":[0,279]," +
               "\"entities\":{\"hashtags\":[{\"text\":\"Toyota\",\"indices\":[23,30]},{\"text\":\"Yaris\",\"indices\":[31,37]},{\"text\":\"3Doors\",\"indices\":[42,49]}," +
               "{\"text\":\"Manual\",\"indices\":[53,60]},{\"text\":\"Hatch\",\"indices\":[84,90]},{\"text\":\"Power\",\"indices\":[114,120]},{\"text\":\"White\"," +
               "\"indices\":[140,146]},{\"text\":\"Bargain\",\"indices\":[188,196]},{\"text\":\"Cars\",\"indices\":[197,202]},{\"text\":\"Sale\",\"indices\":[207,212]}," +
               "{\"text\":\"Liberty\",\"indices\":[216,224]},{\"text\":\"Auto\",\"indices\":[225,230]},{\"text\":\"Mzanzi\",\"indices\":[231,238]},{\"text\":\"SouthAfrica\"," +
               "\"indices\":[239,251]}],\"symbols\":[],\"user_mentions\":[],\"urls\":[],\"media\":[{\"id\":1230713014271111169,\"id_str\":\"1230713014271111169\"," +
               "\"indices\":[256,279],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/media\\/ERRe90QUcAErsm9.jpg\"," +
               "\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/media\\/ERRe90QUcAErsm9.jpg\",\"url\":\"https:\\/\\/t.co\\/96TT7FkYMV\"," +
               "\"display_url\":\"pic.twitter.com\\/96TT7FkYMV\",\"expanded_url\":\"https:\\/\\/twitter.com\\/libertyautoSA\\/status\\/1230713067853377537\\/photo\\/1\"," +
               "\"type\":\"photo\",\"sizes\":{\"large\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"medium\":{\"w\":640," +
               "\"h\":424,\"resize\":\"fit\"},\"small\":{\"w\":640,\"h\":424,\"resize\":\"fit\"}},\"source_status_id\":1230713067853377537," +
               "\"source_status_id_str\":\"1230713067853377537\",\"source_user_id\":4831419297,\"source_user_id_str\":\"4831419297\"}]}," +
               "\"extended_entities\":{\"media\":[{\"id\":1230713014271111169,\"id_str\":\"1230713014271111169\",\"indices\":[256,279]," +
               "\"media_url\":\"http:\\/\\/pbs.twimg.com\\/media\\/ERRe90QUcAErsm9.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/media\\/ERRe90QUcAErsm9.jpg\"," +
               "\"url\":\"https:\\/\\/t.co\\/96TT7FkYMV\",\"display_url\":\"pic.twitter.com\\/96TT7FkYMV\"," +
               "\"expanded_url\":\"https:\\/\\/twitter.com\\/libertyautoSA\\/status\\/1230713067853377537\\/photo\\/1\",\"type\":\"photo\"," +
               "\"sizes\":{\"large\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"}," +
               "\"medium\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"small\":{\"w\":640,\"h\":424,\"resize\":\"fit\"}},\"source_status_id\":1230713067853377537," +
               "\"source_status_id_str\":\"1230713067853377537\",\"source_user_id\":4831419297,\"source_user_id_str\":\"4831419297\"},{\"id\":1230713014396936192," +
               "\"id_str\":\"1230713014396936192\",\"indices\":[256,279],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/media\\/ERRe90uUYAA4SoJ.jpg\"," +
               "\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/media\\/ERRe90uUYAA4SoJ.jpg\",\"url\":\"https:\\/\\/t.co\\/96TT7FkYMV\"," +
               "\"display_url\":\"pic.twitter.com\\/96TT7FkYMV\",\"expanded_url\":\"https:\\/\\/twitter.com\\/libertyautoSA\\/status\\/1230713067853377537\\/photo\\/1\"," +
               "\"type\":\"photo\",\"sizes\":{\"small\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"large\":{\"w\":640," +
               "\"h\":424,\"resize\":\"fit\"},\"medium\":{\"w\":640,\"h\":424,\"resize\":\"fit\"}},\"source_status_id\":1230713067853377537," +
               "\"source_status_id_str\":\"1230713067853377537\",\"source_user_id\":4831419297,\"source_user_id_str\":\"4831419297\"},{\"id\":1230713014325673984," +
               "\"id_str\":\"1230713014325673984\",\"indices\":[256,279],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/media\\/ERRe90dVAAAtxDI.jpg\"," +
               "\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/media\\/ERRe90dVAAAtxDI.jpg\",\"url\":\"https:\\/\\/t.co\\/96TT7FkYMV\"," +
               "\"display_url\":\"pic.twitter.com\\/96TT7FkYMV\",\"expanded_url\":\"https:\\/\\/twitter.com\\/libertyautoSA\\/status\\/1230713067853377537\\/photo\\/1\"," +
               "\"type\":\"photo\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"small\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"large\":{\"w\":640," +
               "\"h\":424,\"resize\":\"fit\"},\"medium\":{\"w\":640,\"h\":424,\"resize\":\"fit\"}},\"source_status_id\":1230713067853377537," +
               "\"source_status_id_str\":\"1230713067853377537\",\"source_user_id\":4831419297,\"source_user_id_str\":\"4831419297\"},{\"id\":1230713014292107266," +
               "\"id_str\":\"1230713014292107266\",\"indices\":[256,279],\"media_url\":\"http:\\/\\/pbs.twimg.com\\/media\\/ERRe90VU0AICrwr.jpg\"," +
               "\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/media\\/ERRe90VU0AICrwr.jpg\",\"url\":\"https:\\/\\/t.co\\/96TT7FkYMV\"," +
               "\"display_url\":\"pic.twitter.com\\/96TT7FkYMV\",\"expanded_url\":\"https:\\/\\/twitter.com\\/libertyautoSA\\/status\\/1230713067853377537\\/photo\\/1\"," +
               "\"type\":\"photo\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"small\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"medium\":{\"w\":640," +
               "\"h\":424,\"resize\":\"fit\"},\"large\":{\"w\":640,\"h\":424,\"resize\":\"fit\"}},\"source_status_id\":1230713067853377537," +
               "\"source_status_id_str\":\"1230713067853377537\",\"source_user_id\":4831419297,\"source_user_id_str\":\"4831419297\"}]},\"metadata\":{\"iso_language_code\":\"en\"," +
               "\"result_type\":\"recent\"},\"source\":\"\\u003ca href=\\\"https:\\/\\/ifttt.com\\\" rel=\\\"nofollow\\\"\\u003eIFTTT\\u003c\\/a\\u003e\"," +
               "\"in_reply_to_status_id\":null,\"in_reply_to_status_id_str\":null,\"in_reply_to_user_id\":null,\"in_reply_to_user_id_str\":null," +
               "\"in_reply_to_screen_name\":null,\"user\":{\"id\":4831419297,\"id_str\":\"4831419297\",\"name\":\"0740956652 Liberty Auto SA\",\"screen_name\":\"libertyautoSA\"," +
               "\"location\":\"Johannesburg, South Africa\",\"description\":\"0740956652 Liberty Auto (Auto Dealership) libertyautodeals@gmail.com  Johannesburg \\nOver 12,000 Certified Used Cars on Bank Finance with No Deposit\"," +
               "\"url\":\"https:\\/\\/t.co\\/ORujraAxbx\",\"entities\":{\"url\":{\"urls\":[{\"url\":\"https:\\/\\/t.co\\/ORujraAxbx\"," +
               "\"expanded_url\":\"https:\\/\\/libertyautosa.blogspot.com\\/\",\"display_url\":\"libertyautosa.blogspot.com\",\"indices\":[0,23]}]}," +
               "\"description\":{\"urls\":[]}},\"protected\":false,\"followers_count\":1122,\"friends_count\":4522,\"listed_count\":3," +
               "\"created_at\":\"Thu Jan 21 02:19:10 +0000 2016\",\"favourites_count\":27,\"utc_offset\":null,\"time_zone\":null,\"geo_enabled\":true,\"verified\":false," +
               "\"statuses_count\":19302,\"lang\":null,\"contributors_enabled\":false,\"is_translator\":false,\"is_translation_enabled\":false," +
               "\"profile_background_color\":\"000000\",\"profile_background_image_url\":\"http:\\/\\/abs.twimg.com\\/images\\/themes\\/theme1\\/bg.png\"," +
               "\"profile_background_image_url_https\":\"https:\\/\\/abs.twimg.com\\/images\\/themes\\/theme1\\/bg.png\",\"profile_background_tile\":false," +
               "\"profile_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/1156376374728413186\\/nvbguuoL_normal.jpg\"," +
               "\"profile_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_images\\/1156376374728413186\\/nvbguuoL_normal.jpg\"," +
               "\"profile_banner_url\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/4831419297\\/1506589642\",\"profile_link_color\":\"FF691F\"," +
               "\"profile_sidebar_border_color\":\"000000\",\"profile_sidebar_fill_color\":\"000000\",\"profile_text_color\":\"000000\",\"profile_use_background_image\":false," +
               "\"has_extended_profile\":true,\"default_profile\":false,\"default_profile_image\":false,\"following\":false,\"follow_request_sent\":false,\"notifications\":false," +
               "\"translator_type\":\"none\"},\"geo\":null,\"coordinates\":null,\"place\":null,\"contributors\":null,\"is_quote_status\":false,\"retweet_count\":0," +
               "\"favorite_count\":0,\"favorited\":false,\"retweeted\":false,\"possibly_sensitive\":false,\"lang\":\"en\"},{\"created_at\":\"Fri Feb 21 08:39:21 +0000 2020\"," +
               "\"id\":1230773993419837441,\"id_str\":\"1230773993419837441\"," +
               "\"full_text\":\"RT libertyautoSA: 2002 #Toyota #TAZZ #Conquest 1600RS #Hatch Back \\n#Manual Transmission 180,000km Front #Wheel Drive Great Buy (#Silver) \\n\\nR55,900\\n\\nOver 12,000 #Bargain #Cars For #Sale at #Liberty #Auto #Mzanzi #SouthAfrica \\n\\nWhats App 074 | 095 | 6652 #\\u2026 https:\\/\\/t.co\\/rcUPvbCxMi\"," +
               "\"truncated\":false,\"display_text_range\":[0,279],\"entities\":{\"hashtags\":[{\"text\":\"Toyota\",\"indices\":[23,30]},{\"text\":\"TAZZ\"," +
               "\"indices\":[31,36]},{\"text\":\"Conquest\",\"indices\":[37,46]},{\"text\":\"Hatch\",\"indices\":[54,60]},{\"text\":\"Manual\"," +
               "\"indices\":[67,74]},{\"text\":\"Wheel\",\"indices\":[104,110]},{\"text\":\"Silver\",\"indices\":[128,135]},{\"text\":\"Bargain\",\"indices\":[160,168]}," +
               "{\"text\":\"Cars\",\"indices\":[169,174]},{\"text\":\"Sale\",\"indices\":[179,184]},{\"text\":\"Liberty\",\"indices\":[188,196]},{\"text\":\"Auto\"," +
               "\"indices\":[197,202]},{\"text\":\"Mzanzi\",\"indices\":[203,210]},{\"text\":\"SouthAfrica\",\"indices\":[211,223]}],\"symbols\":[],\"user_mentions\":[]," +
               "\"urls\":[],\"media\":[{\"id\":1230708879727550464,\"id_str\":\"1230708879727550464\",\"indices\":[256,279]," +
               "\"media_url\":\"http:\\/\\/pbs.twimg.com\\/media\\/ERRbNJ4UYAAC1df.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/media\\/ERRbNJ4UYAAC1df.jpg\"," +
               "\"url\":\"https:\\/\\/t.co\\/rcUPvbCxMi\",\"display_url\":\"pic.twitter.com\\/rcUPvbCxMi\"," +
               "\"expanded_url\":\"https:\\/\\/twitter.com\\/libertyautoSA\\/status\\/1230708896571879429\\/photo\\/1\",\"type\":\"photo\",\"sizes\":{\"thumb\":{\"w\":150," +
               "\"h\":150,\"resize\":\"crop\"},\"large\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"medium\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"small\":{\"w\":640," +
               "\"h\":424,\"resize\":\"fit\"}},\"source_status_id\":1230708896571879429,\"source_status_id_str\":\"1230708896571879429\",\"source_user_id\":4831419297," +
               "\"source_user_id_str\":\"4831419297\"}]},\"extended_entities\":{\"media\":[{\"id\":1230708879727550464,\"id_str\":\"1230708879727550464\",\"indices\":[256,279]," +
               "\"media_url\":\"http:\\/\\/pbs.twimg.com\\/media\\/ERRbNJ4UYAAC1df.jpg\",\"media_url_https\":\"https:\\/\\/pbs.twimg.com\\/media\\/ERRbNJ4UYAAC1df.jpg\"," +
               "\"url\":\"https:\\/\\/t.co\\/rcUPvbCxMi\",\"display_url\":\"pic.twitter.com\\/rcUPvbCxMi\"," +
               "\"expanded_url\":\"https:\\/\\/twitter.com\\/libertyautoSA\\/status\\/1230708896571879429\\/photo\\/1\",\"type\":\"photo\",\"sizes\":{\"thumb\":{\"w\":150," +
               "\"h\":150,\"resize\":\"crop\"},\"large\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"medium\":{\"w\":640,\"h\":424,\"resize\":\"fit\"},\"small\":{\"w\":640," +
               "\"h\":424,\"resize\":\"fit\"}},\"source_status_id\":1230708896571879429,\"source_status_id_str\":\"1230708896571879429\",\"source_user_id\":4831419297," +
               "\"source_user_id_str\":\"4831419297\"}]},\"metadata\":{\"iso_language_code\":\"en\",\"result_type\":\"recent\"}," +
               "\"source\":\"\\u003ca href=\\\"https:\\/\\/ifttt.com\\\" rel=\\\"nofollow\\\"\\u003eIFTTT\\u003c\\/a\\u003e\",\"in_reply_to_status_id\":null," +
               "\"in_reply_to_status_id_str\":null,\"in_reply_to_user_id\":null,\"in_reply_to_user_id_str\":null,\"in_reply_to_screen_name\":null,\"user\":{\"id\":4831419297," +
               "\"id_str\":\"4831419297\",\"name\":\"0740956652 Liberty Auto SA\",\"screen_name\":\"libertyautoSA\",\"location\":\"Johannesburg, South Africa\"," +
               "\"description\":\"0740956652 Liberty Auto (Auto Dealership) libertyautodeals@gmail.com  Johannesburg \\nOver 12,000 Certified Used Cars on Bank Finance with No Deposit\"," +
               "\"url\":\"https:\\/\\/t.co\\/ORujraAxbx\",\"entities\":{\"url\":{\"urls\":[{\"url\":\"https:\\/\\/t.co\\/ORujraAxbx\"," +
               "\"expanded_url\":\"https:\\/\\/libertyautosa.blogspot.com\\/\",\"display_url\":\"libertyautosa.blogspot.com\"," +
               "\"indices\":[0,23]}]},\"description\":{\"urls\":[]}},\"protected\":false,\"followers_count\":1122,\"friends_count\":4522,\"listed_count\":3," +
               "\"created_at\":\"Thu Jan 21 02:19:10 +0000 2016\",\"favourites_count\":27,\"utc_offset\":null,\"time_zone\":null,\"geo_enabled\":true,\"verified\":false," +
               "\"statuses_count\":19302,\"lang\":null,\"contributors_enabled\":false,\"is_translator\":false,\"is_translation_enabled\":false," +
               "\"profile_background_color\":\"000000\",\"profile_background_image_url\":\"http:\\/\\/abs.twimg.com\\/images\\/themes\\/theme1\\/bg.png\"," +
               "\"profile_background_image_url_https\":\"https:\\/\\/abs.twimg.com\\/images\\/themes\\/theme1\\/bg.png\",\"profile_background_tile\":false," +
               "\"profile_image_url\":\"http:\\/\\/pbs.twimg.com\\/profile_images\\/1156376374728413186\\/nvbguuoL_normal.jpg\"," +
               "\"profile_image_url_https\":\"https:\\/\\/pbs.twimg.com\\/profile_images\\/1156376374728413186\\/nvbguuoL_normal.jpg\"," +
               "\"profile_banner_url\":\"https:\\/\\/pbs.twimg.com\\/profile_banners\\/4831419297\\/1506589642\",\"profile_link_color\":\"FF691F\"," +
               "\"profile_sidebar_border_color\":\"000000\",\"profile_sidebar_fill_color\":\"000000\",\"profile_text_color\":\"000000\",\"profile_use_background_image\":false," +
               "\"has_extended_profile\":true,\"default_profile\":false,\"default_profile_image\":false,\"following\":false,\"follow_request_sent\":false,\"notifications\":false," +
               "\"translator_type\":\"none\"},\"geo\":null,\"coordinates\":null,\"place\":null,\"contributors\":null,\"is_quote_status\":false,\"retweet_count\":0," +
               "\"favorite_count\":0,\"favorited\":false,\"retweeted\":false,\"possibly_sensitive\":false,\"lang\":\"en\"}],\"search_metadata\":{\"completed_in\":0.023," +
               "\"max_id\":1230773998075531264,\"max_id_str\":\"1230773998075531264\",\"next_results\":\"?max_id=1230773993419837440&q=cars&geocode=-26.276138500000002%2C28.00929511161209%2C30.37km&lang=en&count=2&include_entities=1&result_type=recent\"," +
               "\"query\":\"cars\",\"refresh_url\":\"?since_id=1230773998075531264&q=cars&geocode=-26.276138500000002%2C28.00929511161209%2C30.37km&lang=en&result_type=recent&include_entities=1\",\"count\":2,\"since_id\":0,\"since_id_str\":\"0\"}}";
    }
}